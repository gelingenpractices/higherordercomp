import { combineReducers } from 'redux';
import changeAuthReducer from './reducer_auth';

const rootReducer = combineReducers({
  authenticated: changeAuthReducer
});

export default rootReducer;
